/**
 * Created by Chris Tofts on 29/03/2016.
 */
var express = require('express');
var basex = require('basex');
var cheerio = require('cheerio');
var pd = require('pretty-data').pd;
var router = express.Router();
var os = require('os');

var client = new basex.Session("localhost", 1984, "admin", "admin");
client.execute("OPEN ColensoXml");


router.get('/text', function (req, res, next) {
    var reply = "There are no documents matching " + req.query.title + ".";
    var resultArray;
    if (req.query.title == null) {
        res.render('searchResults', {title: 'Search', search: title, reply: reply, result: resultArray});
    }

    else{
        reply = "";

        var title = req.query.title;
        var opString = "";
        if (title.indexOf("AND") != -1) {
            andArray = title.split(" AND ");
            for (var i = 0; i < andArray.length; i++) {
                if (i != andArray.length - 1) {
                    opString += '$doc/descendant-or-self::*[text() contains text "' + andArray[i] + '"] intersect '
                } else {
                    opString += '$doc/descendant-or-self::*[text() contains text "' + andArray[i] + '"]'
                }
            }
            resultArray = client.query("declare default element namespace 'http://www.tei-c.org/ns/1.0'; for $doc in collection('ColensoXml') where " + opString + " return $doc");


        }

        else if (title.indexOf("OR") != -1) {
            orsArray = title.split(" OR ");
            for (var i = 0; i < orsArray.length; i++) {
                if (i != orsArray.length - 1) {
                    opString += '$doc/descendant-or-self::*[text() contains text "' + orsArray[i] + '"] union '
                } else {
                    opString += '$doc/descendant-or-self::*[text() contains text "' + orsArray[i] + '"]'
                }
            }
            resultArray = client.query("declare default element namespace 'http://www.tei-c.org/ns/1.0'; for $doc in collection('ColensoXml') where " + opString + " return $doc");
        }

        else if (title.indexOf("NOT") != -1) {
            notArray = title.split(" NOT ");
            for (var i = 0; i < notArray.length; i++) {
                if (i != notArray.length - 1) {
                    opString += '$doc/descendant-or-self::*[text() contains text "' + notArray[i] + '"] except '
                } else {
                    opString += '$doc/descendant-or-self::*[text() contains text "' + notArray[i] + '"]'
                }
            }
            resultArray = client.query("declare default element namespace 'http://www.tei-c.org/ns/1.0'; for $doc in collection('ColensoXml') where " + opString + " return $doc");

        }
        else {
            opString = '$doc/descendant-or-self::*[text() contains text "' + title + '"]'
            resultArray = client.query("declare default element namespace 'http://www.tei-c.org/ns/1.0'; for $doc in collection('ColensoXml') where " + opString + " return $doc");
        }


        resultArray.results( function(error, data){
            if(error){
                console.error(error);
            }
            else{
                var title = data.result;
                if(title.length < 1){
                    reply = "There are no results for " + req.query.title + "."
                    res.render('searchResults', {title: 'Search', search: title, reply: reply, result: ""});
                }
                else {
                    res.render('searchResults', {title: 'Search', search: title, reply: reply, result: title});
                }
            }
        })
    }





});

router.get('/xquery', function(req, res, next){
    var uri = req.query.text;
    var resultArray;
    resultArray = client.query("declare default element namespace 'http://www.tei-c.org/ns/1.0'; " + uri + "");
        resultArray.results(function(err, result){
            if(err){
                console.error(err)
            }
            else{
                var title = result.result;
                if(title.length < 1){
                    reply = "There are no results for " + req.query.text + "."
                    res.render('searchResults', {title: 'Search', search: title, reply: reply, result: ""});
                }
                else {
                    var title = result.result;
                    res.render('searchResults', {title: 'search', search: "", reply: "", result: title})
                }
            }
        }
    )
})


router.get('/text/xmlFile/*', function(req, res, next){
    var searchQuery = req.originalUrl;
    var lastSegment = searchQuery.split("/").pop();
    var cleanVersion = decodeURIComponent(lastSegment);

    client.execute("XQUERY declare default element namespace 'http://www.tei-c.org/ns/1.0'; for $doc in collection('ColensoXml') where $doc//title/text() contains text '" + cleanVersion + "' return $doc"
        , function(err, result){
            if(err){
                console.error(err);
        }
        else{


                res.render('searchXml' ,{title: cleanVersion, result: result.result})
            }
    })
});

router.get('/xquery/xmlFile/*', function(req, res, next){
    var searchQuery = req.originalUrl;
    var lastSegment = searchQuery.split("/").pop();
    lastSegment = decodeURIComponent(lastSegment);


    client.execute("XQUERY declare default element namespace 'http://www.tei-c.org/ns/1.0'; for $doc in collection('ColensoXml') where $doc//title/text() contains text '" + lastSegment + "' return $doc"
        , function(err, result){
            if(err){
                console.error(err);
            }
            else{
                var path = req.query.path;
                res.render('searchXml' ,{title: lastSegment, result: result.result})
            }
        })
});





















module.exports = router;