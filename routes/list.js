var express = require('express');
var basex = require('basex');
var cheerio = require('cheerio');
var pd = require('pretty-data').pd;
var vkbeautify = require('vkbeautify');
var router = express.Router();

var client = new basex.Session("localhost", 1984, "admin", "admin");
client.execute("OPEN ColensoXml");

/* gets the full list page */


router.get('/files/*', function(req, res, next){
    var xml = req.originalUrl;
    var lastSegment = xml.split('/').pop();
    client.execute("XQUERY declare default element namespace 'http://www.tei-c.org/ns/1.0'; for $doc in collection('ColensoXml') where matches(document-uri($doc), '" + lastSegment + "') return $doc"
        , function(error, result){
            if(error){
                console.log("i broke");
                console.error(error);
                res.render('error', {error: error});
            }
            else {

                var searchArray = result.result.split("\n");
                var title = result.result.substring(result.result.indexOf("<title>") + 7,result.result.indexOf("</title>"));
                res.render('xmlFile', {title: title, result: result.result});
            }
        }
    )
});/**
 * Created by Chris Tofts on 21/03/2016.
 */


module.exports = router;
