var express = require('express');
var basex = require('basex');
var cheerio = require('cheerio');
var pd = require('pretty-data').pd;
var router = express.Router();
var os = require('os');

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {title: 'Colenso', place: 'Wellington'});
});


/* gets the list page */
router.get('/list', function (req, res, next) {
    client.execute("XQUERY db:list('ColensoXml')", function (error, result) {
        if (error) {
            console.error(error);
        }
        else {
            var list = result.result;
            var arrayList = list.split(os.EOL);

            res.render('list', {title: 'List', place: 'Otaki', list: arrayList})
        }
    })
});

var client = new basex.Session("localhost", 1984, "admin", "admin");
client.execute("OPEN ColensoXml");


router.get('/search', function (req, res, next) {

    res.render('search', {title: 'search'});


    });


router.get('/download/*', function(req, res, next){
    var uri = req.originalUrl;
    var lastSegment = uri.split("/").pop();
    var cleanSegment = decodeURIComponent(lastSegment);
    var oneWord = cleanSegment.replace(/\s+/g, '');

    client.execute("XQUERY declare default element namespace 'http://www.tei-c.org/ns/1.0'; for $doc in collection('ColensoXml') where $doc//title/text() contains text '" + cleanSegment + "' return $doc",
    function(err, result){
        if(err){
            console.error(err);
        }
        else{

            var doc = result.result;
            res.writeHead(200, {
                'Content-Disposition': 'attachment; filename= header'
            });
            res.write(doc);
            res.end();
        }

    })

})









module.exports = router;
